package com.example.loginandregisterretrofit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyView> {
    List<ProductModel> data;
    Context context;
    Activity activity;

    public ProductAdapter(Context context, List<ProductModel> data){
        this.data = data;
        this.context = context;

    }

    @NonNull
    @Override
    public ProductAdapter.MyView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_product, parent, false);
        return new MyView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductAdapter.MyView holder, final int position) {
        holder.name.setText(data.get(position).getName());
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int getID = data.get(position).getId();
                String name = data.get(position).getName();
                String img = data.get(position).getImageurl();
                Intent intent = new Intent(context, UpdateProduct.class);
                intent.putExtra("id", getID);
                intent.putExtra("name", name);
                intent.putExtra("imageurl", img);
                context.startActivity(intent);
            }
        });
        Picasso.with(context).load(data.get(position).getImageurl()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyView extends RecyclerView.ViewHolder {
        TextView name;
        LinearLayout linearLayout;
        ImageView imageView;
        public MyView(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.tvNameProduct);
            linearLayout = itemView.findViewById(R.id.main_layout);
            imageView = itemView.findViewById(R.id.imageproduct);
        }
    }
}
