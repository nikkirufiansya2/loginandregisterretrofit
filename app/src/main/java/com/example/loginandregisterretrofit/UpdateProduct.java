package com.example.loginandregisterretrofit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UpdateProduct extends AppCompatActivity {
    EditText etname;
    Button ubah, hapus, selectImage;
    private ImageView img;
    final int IMAGE_REQUEST_CODE = 999;
    private Uri filepath;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_product);
        String name = getIntent().getStringExtra("name");
        String image = getIntent().getStringExtra("imageurl");
        selectImage = findViewById(R.id.btn_select);
        img = findViewById(R.id.selectImage);

        Picasso.with(this).load(image).into(img);
        etname = findViewById(R.id.productName);
        etname.setText(name);
        ubah = findViewById(R.id.btnUpdate);
        selectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(UpdateProduct.this,new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},IMAGE_REQUEST_CODE);
            }
        });
        ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateData();
            }
        });
        hapus = findViewById(R.id.btndelete);
        hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteData();
            }
        });
    }

    void deleteData() {
        int id = getIntent().getIntExtra("id", 0);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(InterfaceAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        InterfaceAPI api = retrofit.create(InterfaceAPI.class);
        Call<ValueProductModel> call = api.deleteProduct(id);
        call.enqueue(new Callback<ValueProductModel>() {
            @Override
            public void onResponse(Call<ValueProductModel> call, Response<ValueProductModel> response) {
                startActivity(new Intent(UpdateProduct.this, ProductActivity.class));
            }

            @Override
            public void onFailure(Call<ValueProductModel> call, Throwable t) {

            }
        });
    }

    void updateData() {
        String nama = etname.getText().toString();
        int id = getIntent().getIntExtra("id", 0);
        String imgdata=imgToString(bitmap);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(InterfaceAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        InterfaceAPI api = retrofit.create(InterfaceAPI.class);
        Call<ValueProductModel> call = api.updateProduct(id, nama, imgdata);
        call.enqueue(new Callback<ValueProductModel>() {
            @Override
            public void onResponse(Call<ValueProductModel> call, Response<ValueProductModel> response) {
                startActivity(new Intent(UpdateProduct.this, ProductActivity.class));
            }

            @Override
            public void onFailure(Call<ValueProductModel> call, Throwable t) {

            }
        });

    }

    //permission untuk membuka file manager di hp
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == IMAGE_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(new Intent(Intent.ACTION_PICK));
                intent.setType("image/*");

                startActivityForResult(Intent.createChooser(intent, "select image"), IMAGE_REQUEST_CODE);

            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            filepath = data.getData();
            try {
                InputStream inputStream = getContentResolver().openInputStream(filepath);
                bitmap = BitmapFactory.decodeStream(inputStream);
                img.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //mengubah image menjadi string
    private String imgToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imgbytes = byteArrayOutputStream.toByteArray();
        String encodeimg = Base64.encodeToString(imgbytes, Base64.DEFAULT);
        return encodeimg;
    }
}