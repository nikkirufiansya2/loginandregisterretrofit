package com.example.loginandregisterretrofit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ProductActivity extends AppCompatActivity {
    List<ProductModel> data;
    RecyclerView recyclerView;
    ProductAdapter productAdapter;
    FloatingActionButton add;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        data = new ArrayList<>();
        getResponse();

        add = findViewById(R.id.floatingActionButton);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductActivity.this, AddProduct.class));
            }
        });

    }


    //method untuk get API
    void getResponse() {
        //membuat variable retrofit dan set BASE_URL, Converter JSON
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(InterfaceAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //membuat variable interface API
        InterfaceAPI api = retrofit.create(InterfaceAPI.class);
        //membuat feedback call dan get method di interface api
        Call<ValueProductModel> call = api.getDataProdouct();
        call.enqueue(new Callback<ValueProductModel>() {
            @Override
            public void onResponse(Call<ValueProductModel> call, Response<ValueProductModel> response) {
                data = response.body().getData();

                productAdapter = new ProductAdapter(ProductActivity.this, data);
                recyclerView.setAdapter(productAdapter);
            }

            @Override
            public void onFailure(Call<ValueProductModel> call, Throwable t) {

            }
        });


    }





}