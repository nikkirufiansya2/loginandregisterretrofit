package com.example.loginandregisterretrofit;


//mengambil per data dari json, jadi 1 data itu dipanggil jadi 1 ProductModel

//        {
//        "id": "1",
//        "name": "buku novel"
//        }

public class ProductModel {
    int id;
    String name;

    String imageurl;

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
