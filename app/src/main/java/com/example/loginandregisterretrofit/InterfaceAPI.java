package com.example.loginandregisterretrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface InterfaceAPI {


    String BASE_URL = "http://192.168.1.14/apiuser/";

    //consume API daftar.php
    @FormUrlEncoded
    @POST("daftar.php")
    Call<String> DaftarUser(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password
    );

    //consume API login.php
    @FormUrlEncoded
    @POST("login.php")
    Call<String> LoginUser(
            @Field("email") String email,
            @Field("password") String password
    );


    //consume API Product.php - mengambil data menggunakan controller php
    @GET("product.php")
    Call<ValueProductModel> getDataProdouct();

    //consume API UPDATE product
    @FormUrlEncoded
    @POST("update_product.php")
    Call<ValueProductModel> updateProduct(@Field("id") int id,@Field("name") String name, @Field("imageurl") String imageurl);

    //consume API DELETE product
    @FormUrlEncoded
    @POST("delete_product.php")
    Call<ValueProductModel> deleteProduct(@Field("id") int id);

    @FormUrlEncoded
    @POST("insert_product.php")
    Call<ValueProductModel> addProduct(@Field("name") String name, @Field("imageurl") String imageurl);

}
