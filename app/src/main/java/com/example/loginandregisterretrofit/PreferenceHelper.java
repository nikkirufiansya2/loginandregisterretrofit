package com.example.loginandregisterretrofit;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {
    //myPreference adalah keyname jadi nama boleh terserah
    private final String namePreference = "myPreference";
    private final String NAME = "name";
    private final String EMAIL = "email";
    private SharedPreferences app_prefs;
    private Context context;


    //method public yg di gunkankan di class lain untuk mengambil method2 lainnya
    public PreferenceHelper(Context context) {
        //shared adalah nama preference
        app_prefs = context.getSharedPreferences("shared",
                Context.MODE_PRIVATE);
        this.context = context;
    }

    //menyimpan session apakah user login sudah pernah login atau tidak
    public void putIsLogin(boolean loginorout) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putBoolean(namePreference, loginorout);
        edit.commit();
    }

    //mengambil data atau memberikan nilai false ketika tidak ada data
    public boolean getIsLogin() {
        return app_prefs.getBoolean(namePreference, false);
    }

    //tempat nyimpan data
    public void putName(String loginorout) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(NAME, loginorout);
        edit.commit();
    }

    // method buat munculkan data dari Putname
    public String getName() {
        return app_prefs.getString(NAME, "");
    }

    public void putEmail(String loginorout) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(EMAIL, loginorout);
        edit.commit();
    }

    public String getEmail() {
        return app_prefs.getString(EMAIL, "");
    }
}
