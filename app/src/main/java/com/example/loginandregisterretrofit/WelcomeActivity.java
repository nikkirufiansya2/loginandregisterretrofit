package com.example.loginandregisterretrofit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class WelcomeActivity extends AppCompatActivity {

    private TextView tvname,tvEmail;
    private Button btnlogout, btnProduct;
    private PreferenceHelper preferenceHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        preferenceHelper = new PreferenceHelper(this);
        preferenceHelper.getIsLogin();
        tvname = findViewById(R.id.tvname);
        tvEmail = findViewById(R.id.tvEmail);

        tvname.setText("hello " + preferenceHelper.getName());
        tvEmail.setText(preferenceHelper.getEmail());
        btnlogout = findViewById(R.id.btnLogout);
        btnProduct = findViewById(R.id.btnProduct);
        btnProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WelcomeActivity.this, ProductActivity.class));
            }
        });
        btnlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferenceHelper.putIsLogin(false);
                startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
            }
        });

    }
}