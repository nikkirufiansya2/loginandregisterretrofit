package com.example.loginandregisterretrofit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MainActivity extends AppCompatActivity {
    EditText etEmail, etPassword;
    Button Login;
    private PreferenceHelper preferenceHelper;
    TextView daftar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferenceHelper = new PreferenceHelper(this);

            if (preferenceHelper.getIsLogin()) {
                Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                this.finish();
            }

            daftar = findViewById(R.id.tvdaftar);
            etEmail = findViewById(R.id.etemail);
            etPassword = findViewById(R.id.etpassword);
            Login = findViewById(R.id.btnLogin);
            daftar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, DaftarActivity.class));
                }
            });
            Login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String email = etEmail.getText().toString().trim();
                    final String password = etPassword.getText().toString().trim();

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(InterfaceAPI.BASE_URL)
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .build();
                    //Call memberikan fedback respon dengan memanggil url json dalam bentuk method di class Interface API

                    InterfaceAPI api = retrofit.create(InterfaceAPI.class);

                    Call<String> call = api.LoginUser(email, password);
                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    System.out.println(response.body());
                                    String jsonRespon = response.body();
                                    parseLoginData(jsonRespon);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {

                        }
                    });
                }
            });
        }

        //memparsing json respon().body() dan mengambil data apa bila status output json = true
        private void parseLoginData (String response){
            try {
                JSONObject jsonObject = new JSONObject(response);
                System.out.println(jsonObject);
                if (jsonObject.getString("status").equals("true")) {
                    saveInfo(response);
                    System.out.println(response);
                    Toast.makeText(MainActivity.this, "Login Successfully!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
                    startActivity(intent);

                } else {
                    Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    //
    private void saveInfo(String response){
        System.out.println(response);
        preferenceHelper.putIsLogin(true);
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equals("true")) {
                JSONArray dataArray = jsonObject.getJSONArray("data");
                System.out.println(dataArray);

                for (int i = 0; i < dataArray.length(); i++) {
                    System.out.println("panjang array : " + dataArray.length());
                    JSONObject dataobj = dataArray.getJSONObject(i);
                    preferenceHelper.putName(dataobj.getString("name"));
                    preferenceHelper.putEmail(dataobj.getString("email"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
